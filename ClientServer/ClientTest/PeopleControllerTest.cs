﻿using NUnit.Framework;
using SharedModel;
using System;
using System.Collections.Generic;

namespace Server.Tests
{
    [TestFixture]
    public class PeopleControllerTests
    {
        private PeopleController _controller;

        [SetUp]
        public void Setup()
        {
            _controller = new PeopleController();
        }

        #region Тесты методов

        /// <summary>
        /// Тест: Получение человека по идентификатору (должен вернуть человека).
        /// </summary>
        [Test]
        public void GetPeopleById_PersonId_ReturnsPerson()
        {
            // Arrange
            int personId = 1;

            // Act
            var result = _controller.GetPeopleById(personId);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<People>(result);
            Assert.AreEqual(personId, result.PeopleId);
        }

        /// <summary>
        /// Тест: Получение человека по несуществующему идентификатору (должен вернуть null).
        /// </summary>
        [Test]
        public void GetPeopleById_nonPersonId_ReturnsNull()
        {
            // Arrange
            int nonPersonId = 999;

            // Act
            var result = _controller.GetPeopleById(nonPersonId);

            // Assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Тест: Получение списка всех людей (должен вернуть список).
        /// </summary>
        [Test]
        public void GetAllPeople_ReturnsListOfPeople()
        {
            // Act
            var result = _controller.GetAllPeople();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<List<People>>(result);
        }

        /// <summary>
        /// Тест: Сохранение человека с корректными данными (должен вернуть сохраненного человека).
        /// </summary>
        [Test]
        public void SavePeople_ValidInput_ReturnsSavedPerson()
        {
            // Arrange
            var person = new People
            {
                FirstName = "John",
                LastName = "Doe",
                JobTitle = "Software Engineer"
            };

            // Act
            var result = _controller.SavePeople(person);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<People>(result);
            Assert.AreNotEqual(0, result.PeopleId);
            Assert.AreEqual(person.FirstName, result.FirstName);
            Assert.AreEqual(person.LastName, result.LastName);
            Assert.AreEqual(person.JobTitle, result.JobTitle);
        }

        /// <summary>
        /// Тест: Удаление человека по существующему идентификатору (должен вернуть true).
        /// </summary>
        [Test]
        public void DeletePeople_personId_ReturnsTrue()
        {
            // Arrange
            int personId = 2;

            // Act
            var result = _controller.DeletePeople(personId);

            // Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Тест: Удаление человека по несуществующему идентификатору (должен вернуть false).
        /// </summary>
        [Test]
        public void DeletePeople_nonPersonId_ReturnsFalse()
        {
            // Arrange
            int nonPersonId = 999;

            // Act
            var result = _controller.DeletePeople(nonPersonId);

            // Assert
            Assert.IsFalse(result);
        }

        #endregion
    }
}
