using NUnit.Framework;
using SharedModel;
using System;
using System.Collections.Generic;

namespace Server.Tests
{
    [TestFixture]
    public class TaskControllerTests
    {
        private TaskController _controller;

        [SetUp]
        public void Setup()
        {
            _controller = new TaskController();
        }

        #region ����� �������

        /// <summary>
        /// ����: ��������� ������ �� �������������� (������ ������� ������).
        /// </summary>
        [Test]
        public void GetTaskById_taskId_ReturnsTask()
        {
            // Arrange
            int id = 2;

            // Act
            var result = _controller.GetTaskById(id);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<SharedModel.Task>(result);
            Assert.AreEqual(id, result.TaskId);
        }

        /// <summary>
        /// ����: ��������� ������ �� ��������������� �������������� (������ ������� null).
        /// </summary>
        [Test]
        public void GetTaskById_nontaskId_ReturnsNull()
        {
            // Arrange
            int nontaskId = 999;

            // Act
            var result = _controller.GetTaskById(nontaskId);

            // Assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// ����: ��������� ������ ���� ����� (������ ������� ������).
        /// </summary>
        [Test]
        public void GetAllTask_ReturnsListOfTasks()
        {
            // Act
            var result = _controller.GetAllTask();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<List<SharedModel.Task>>(result);
        }

        /// <summary>
        /// ����: ���������� ������ � ����������� ������� (������ ������� ����������� ������).
        /// </summary>
        [Test]
        public void SaveTask_ValidInput_ReturnsSavedTask()
        {
            // Arrange
            var task = new SharedModel.Task
            {
                Title = "test",
                Description = "test",
                Deadline = new DateTime(2024, 12, 12, 22, 0, 0),
                PeopleId = 1
            };

            // Act
            var result = _controller.SaveTask(task);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<SharedModel.Task>(result);
            Assert.AreNotEqual(0, result.TaskId);
            Assert.AreEqual(task.Title, result.Title);
            Assert.AreEqual(task.Description, result.Description);
            Assert.AreEqual(task.Deadline, result.Deadline);
            Assert.AreEqual(task.PeopleId, result.PeopleId);
        }

        /// <summary>
        /// ����: �������� ������ �� ������������� �������������� (������ ������� true).
        /// </summary>
        [Test]
        public void DeleteTask_TaskId_ReturnsTrue()
        {
            // Arrange
            int TaskId = 1;

            // Act
            var result = _controller.DeleteTask(TaskId);

            // Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// ����: �������� ������ �� ��������������� �������������� (������ ������� false).
        /// </summary>
        [Test]
        public void DeleteTask_nontaskId_ReturnsFalse()
        {
            // Arrange
            int nontaskId = 999;

            // Act
            var result = _controller.DeleteTask(nontaskId);

            // Assert
            Assert.IsFalse(result);
        }

        #endregion
    }
}
